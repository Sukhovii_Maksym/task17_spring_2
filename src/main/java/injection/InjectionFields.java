package injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import otherBeans.OtherBeanA;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;

@Component
public class InjectionFields {
    @Autowired
    @Qualifier("obA")
    private OtherBeanA obA;
    @Autowired
    private OtherBeanB obB;
    @Autowired
    private OtherBeanC obC;

    public InjectionFields() {
    }

    public InjectionFields(OtherBeanA obA, OtherBeanB obB, OtherBeanC obC) {
        this.obA = obA;
        this.obB = obB;
        this.obC = obC;
    }

    public OtherBeanA getObA() {
        return obA;
    }

    public void setObA(OtherBeanA obA) {
        this.obA = obA;
    }

    public OtherBeanB getObB() {
        return obB;
    }

    public void setObB(OtherBeanB obB) {
        this.obB = obB;
    }

    public OtherBeanC getObC() {
        return obC;
    }

    public void setObC(OtherBeanC obC) {
        this.obC = obC;
    }

    @Override
    public String toString() {
        return "InjectionFields{" +
                "obA=" + obA +
                ", obB=" + obB +
                ", obC=" + obC +
                '}';
    }
}
