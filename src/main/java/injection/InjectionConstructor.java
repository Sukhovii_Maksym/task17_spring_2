package injection;

import org.springframework.stereotype.Component;
import otherBeans.OtherBeanA;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;

@Component
public class InjectionConstructor {
    private OtherBeanA obA;
    private OtherBeanB obB;
    private OtherBeanC obC;


    public InjectionConstructor() {
    }

    public InjectionConstructor(OtherBeanA obA, OtherBeanB obB, OtherBeanC obC) {
        this.obA = obA;
        this.obB = obB;
        this.obC = obC;
    }

    public OtherBeanA getObA() {
        return obA;
    }

    public void setObA(OtherBeanA obA) {
        this.obA = obA;
    }

    public OtherBeanB getObB() {
        return obB;
    }

    public void setObB(OtherBeanB obB) {
        this.obB = obB;
    }

    public OtherBeanC getObC() {
        return obC;
    }

    public void setObC(OtherBeanC obC) {
        this.obC = obC;
    }

    @Override
    public String toString() {
        return "InjectionConstructor{" +
                "obA=" + obA +
                ", obB=" + obB +
                ", obC=" + obC +
                '}';
    }
}
