package injection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import otherBeans.OtherBeanA;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;

@Component
public class InjectionSetters {
    private OtherBeanA obA;
    private OtherBeanB obB;
    private OtherBeanC obC;

    public InjectionSetters() {
    }

    public OtherBeanA getObA() {
        return obA;
    }

    @Autowired
    @Qualifier("obA")
    public void setObA(OtherBeanA obA) {
        this.obA = obA;
    }

    public OtherBeanB getObB() {
        return obB;
    }

    @Autowired
    @Qualifier("obB")
    public void setObB(OtherBeanB obB) {
        this.obB = obB;
    }

    public OtherBeanC getObC() {
        return obC;
    }

    @Autowired
    @Qualifier("obC")
    public void setObC(OtherBeanC obC) {
        this.obC = obC;
    }

    @Override
    public String toString() {
        return "InjectionSetters{" +
                "obA=" + obA +
                ", obB=" + obB +
                ", obC=" + obC +
                '}';
    }
}
