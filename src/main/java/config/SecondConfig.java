package config;

import beans3.BeanD;
import beans3.BeanF;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan(basePackages = "beans2",
        includeFilters = @Filter(type = FilterType.REGEX, pattern = ".*Flower"))
@ComponentScan(basePackages = "beans3",
        includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BeanD.class, BeanF.class}))
public class SecondConfig {
}
