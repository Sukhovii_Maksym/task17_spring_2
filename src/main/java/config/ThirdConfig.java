package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"injection", "otherBeans", "extraBeans"})
public class ThirdConfig {
}
