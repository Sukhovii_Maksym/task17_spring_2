package beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String data;

    public BeanB() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "data='" + data + '\'' +
                '}';
    }
}
