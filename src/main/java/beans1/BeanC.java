package beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {
    private String data;

    public BeanC() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "data='" + data + '\'' +
                '}';
    }
}
