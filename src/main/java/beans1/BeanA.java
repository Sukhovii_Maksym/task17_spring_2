package beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private String data;

    public BeanA() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "data='" + data + '\'' +
                '}';
    }
}
