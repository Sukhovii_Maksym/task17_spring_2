package extraBeans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(0)
public class bean3 implements Extra {
    @Value("0")
    private String info;

    public bean3() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "bean3{" +
                "info='" + info + '\'' +
                '}';
    }
}
