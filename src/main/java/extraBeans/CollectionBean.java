package extraBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CollectionBean {
    @Autowired
    private List<Extra> beans;

    public void printBeans() {
        beans.forEach(System.out::println);
    }
}
