package beans2;


public class NarcissusFlower {
    private String name;
    private String color;

    public NarcissusFlower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
