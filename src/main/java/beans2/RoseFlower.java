package beans2;


public class RoseFlower {
    private String name;
    private String color;

    public RoseFlower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
