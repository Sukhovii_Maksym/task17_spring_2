package beans3;


public class BeanE {
    private String data;

    public BeanE() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "data='" + data + '\'' +
                '}';
    }
}
