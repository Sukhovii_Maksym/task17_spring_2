package beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String data;

    public BeanD() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "data='" + data + '\'' +
                '}';
    }
}
