package beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanF {
    private String data;

    public BeanF() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "data='" + data + '\'' +
                '}';
    }
}
