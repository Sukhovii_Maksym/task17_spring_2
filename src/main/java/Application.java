import config.FirstConfig;
import config.SecondConfig;
import config.ThirdConfig;
import extraBeans.CollectionBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import otherBeans.OtherBeanB;
import otherBeans.OtherBeanC;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(
                FirstConfig.class, SecondConfig.class, ThirdConfig.class);
        CollectionBean cb = (CollectionBean) context.getBean("collectionBean");
        cb.printBeans();
        OtherBeanC obC1 = (OtherBeanC) context.getBean("otherBeanC");
        OtherBeanC obC2 = (OtherBeanC) context.getBean("otherBeanC");
        System.out.println(obC1.hashCode() + ">---<" + obC2.hashCode() + " Scope(Singleton)");

        OtherBeanB obB1 = (OtherBeanB) context.getBean("otherBeanB");
        OtherBeanB obB2 = (OtherBeanB) context.getBean("otherBeanB");
        System.out.println(obB1.hashCode() + ">---<" + obB2.hashCode() + " Scope(Prototype)");
    }
}
