package otherBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("obA")
@Scope("singleton")
public class OtherBeanA {
    private String data;

    public OtherBeanA() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OtherBeanA{" +
                "data='" + data + '\'' +
                '}';
    }
}
