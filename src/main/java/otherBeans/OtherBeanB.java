package otherBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("obB")
@Scope("prototype")
public class OtherBeanB {
    private String data;

    public OtherBeanB() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OtherBeanB{" +
                "data='" + data + '\'' +
                '}';
    }
}
