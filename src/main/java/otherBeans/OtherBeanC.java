package otherBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("obC")
@Scope("singleton")
public class OtherBeanC {
    private String data;

    public OtherBeanC() {
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OtherBeanC{" +
                "data='" + data + '\'' +
                '}';
    }
}
